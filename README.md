# hw05_final

[![CI](https://github.com/yandex-praktikum/hw05_final/actions/workflows/python-app.yml/badge.svg?branch=master)](https://github.com/yandex-praktikum/hw05_final/actions/workflows/python-app.yml)

<h2 align="center">Проект Yatube</h2>
<h4>Проект Yatube - это социальная сеть, основная функция которой - публикация постов - по сути, соцсеть для ведения блога, который могут комментировать другие люди.</h4>

<h2>Установка:</h2>

<ul>
<li><p>Скопируйте репозиторий в свою папку: git clone git@github.com:DayKotya/hw05_final.git</p></li>

<li><p>Установите виртуальное окружение: python -m venv venv</p></li>

<li><p>Активируйте виртуальное окружение: source venv/Scripts/activate</p></li>

<li><p>Установите зависимости из requirements.txt: pip install -r requirements.txt</p></li>

<li><p>Выполните миграции: python manage.py migrate</p></li>
</li>
<li><p>Запустите сервер: python manage.py runserver</p></li>
</ul>

<h2>Использованные технологии:</h2>

<ul>
<li><p>Python 3.7.9</p></li>
<li><p>Django 2.2.16</p></li>
</ul>
